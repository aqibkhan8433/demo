FROM node:14.16.1

WORKDIR /app

# Install app dependencies
COPY package.json .

# Versions
RUN npm -v
RUN node -v

# install node_modules
RUN npm install

# Bundle app source
COPY . .

# Port to listener

CMD ["npm", "start"]